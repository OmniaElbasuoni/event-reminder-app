package com.example.eventreminderapplication.Events;

import com.google.api.services.tasks.model.Task;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Asynchronously load the tasks.
 *
 * @author Yaniv Inbar
 */
class AsyncLoadTasks extends CommonAsyncTask {

    AsyncLoadTasks(EventsActivity tasksSample) {
        super(tasksSample);
    }

    @Override
    protected void doInBackground() throws IOException {
        List<String> result = new ArrayList<String>();
        List<Task> tasks =
                client.tasks().list("@default").setFields("items/title").setKey("283189451668-2ukvd1ac3qjb2v2hi5k0ldv9ke6d4vel.apps.googleusercontent.com").execute().getItems();
        if (tasks != null) {
            for (Task task : tasks) {
                result.add(task.getTitle());
            }
        } else {
            result.add("No tasks.");
        }
        activity.tasksList = result;
    }

    static void run(EventsActivity tasksSample) {
        new AsyncLoadTasks(tasksSample).execute();
    }
}